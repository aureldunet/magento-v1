FROM debian:buster-slim

RUN apt-get update \
&&  apt-get install -y --no-install-recommends \
software-properties-common \
apt-transport-https \
lsb-release \
ca-certificates \
&&  apt-get update \
&&  apt-get install -y --no-install-recommends \
gnupg \
gnupg1 \
gnupg2 \
ssl-cert \
git \
wget \
curl \
unzip

RUN apt-get update \
&&  apt-get install -y --no-install-recommends \
supervisor

COPY docker/supervisor/supervisord.conf /etc/supervisor/supervisord.conf

COPY docker/supervisor/conf.d/*.conf /etc/supervisor/conf.d/

RUN apt-get update \
&&  curl -sL https://deb.nodesource.com/setup_10.x | bash - \
&&  apt-get install -y --no-install-recommends \
nodejs

RUN apt-get update \
&&  wget -q https://packages.sury.org/php/apt.gpg -O- | apt-key add - \
&&  echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/php.list

RUN export DEBIAN_FRONTEND=noninteractive \
&&  apt-get update \
&&  apt-get install -y \
apache2 \
libapache2-mod-php7.2 \
php7.2 \
php7.2-cli \
php7.2-curl \
php7.2-mbstring \
php7.2-soap \
php7.2-mysql \
php7.2-xml \
php7.2-gd \
php7.2-intl \
php7.2-zip

COPY docker/php/php.ini /etc/php/7.2/apache2/conf.d/

COPY docker/php/php-cli.ini /etc/php/7.2/cli/conf.d/

COPY docker/apache/envvars /etc/apache2/envvars

COPY docker/apache/sites-available/*.conf /etc/apache2/sites-available/

COPY docker/apache/*.conf /etc/apache2/

RUN rm /etc/apache2/sites-enabled/*

RUN ln -sf /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-enabled/000-default.conf

RUN a2enmod rewrite \
&&  a2enmod headers

RUN mkdir -p /var/www/html

RUN rm -rf /var/www/html/*

RUN chown www-data:www-data /var/www/html \
&&  chmod 777 /var/www/html

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer

RUN chmod +x -R /usr/bin/composer

COPY docker/cron/* /etc/cron.d/

RUN chmod +x -R /etc/cron.d/

COPY . /var/www/html

EXPOSE 80

WORKDIR /var/www/html

RUN composer install --no-suggest --no-progress

#RUN npm install

CMD ["/usr/bin/supervisord"]
